<?php
namespace BonsaiCode;
/**
 * Debug utility class
 *
 * Provides methods that are helpful for debugging.
 *
 * @author Alan Davis <alan@bonsaicode.dev>
 * @copyright 2018 BonsaiCode, LLC
 *
 */
class Debug {

	/*
	 *  Constants used in getTypeAbbr, printR, and dumpR
	 */
	const ABBR_ARRAY    = '(a)';
	const ABBR_BOOLEAN  = '(b)';
	const ABBR_DOUBLE   = '(d)';
	const ABBR_INTEGER  = '(i)';
	const ABBR_OBJECT   = '(o)';
	const ABBR_RESOURCE = '(r)';
	const ABBR_STRING   = '(s)';
	const ABBR_UNKNOWN  = '(?)';
	const ABBR_NULL     = 'NULL';

	/**
	 * trace - Generates a trace including parameter values and sends it to the given output.
	 *
	 * @param array $outputDevices Optional.  Can contain 'log', 'page', 'string', 'array' and/or absolute filepath for a custom log file, but cannot use 'string' and 'array' together.
	 *                             Default is 'log'.
	 *				       Must use a log file for very large output instead of error_log because messages get truncated regardless of settings in error log.
	 *
	 * @return mixed Returns output in string if $outputDevices includes 'string', returns output as array if $outputDevices includes 'array', otherwise null is returned.
	 */
	static function trace( array $outputDevices = [ 'log' ] ) {
		$dt        = new \DateTime(); # get time here for beginning of debug
		$pos       = strlen( BASE_DIR ); # remove this many characters from the file paths
		$backtrace = debug_backtrace();
		$out       = [];
		foreach( $backtrace as $i => $functionCall ) {
			# exclude the call to this trace method, so skip over 0 index
			if( $i > 0 ) {
				$args = [];
				foreach( $functionCall['args'] as $a => $arg ) {
					if( is_string( $arg ) ) {
						$args[] = ' String: '.$arg;
					} else if( is_object( $arg ) ) {
						$args[] = ' Object: '.get_class( $arg );
					} else if( is_array( $arg ) ) {
						$args[] = ' Array: '.json_encode( $arg );
					} else {
						$args[] = ' '.gettype( $arg ).': '.$arg;
					}
				}
				# when not calling from a method, class and type won't exist
				$class   = isset( $functionCall['class'] ) ? $functionCall['class'].$functionCall['type'] : '';
				$out[$i] = substr( $functionCall['file'], $pos ).'('.$functionCall['line'].'): '.$class.$functionCall['function'].'('.implode( ', ', $args ).')';
			}
		}
		# add the time at the end of the list because the list is in reverse order of calls
		$out[] = $dt->format( 'Y-m-d H:i:s.u' );

		$returnOutput = null;
		foreach( $outputDevices as $outputDevice ) {
			switch( $outputDevice ) {
				case 'log':
					error_log( print_r( $out, true ) );
					break;
				case 'string':
					$returnOutput = print_r( $out, true );
					break;
				case 'array':
					$returnOutput = $out;
					break;
				case 'page':
					print_r( $out )."\n";
					break;
				default:
					if( strpos( $outputDevice, DIRECTORY_SEPARATOR ) === 0 ) {
						$status = error_log( print_r( $out, true )."\n", 3, $outputDevice );
						if( ! $status ) {
							error_log( "Error writing to $outputDevice" );
						}
					}
					break;
			}
		}
		return $returnOutput;
	}

	/**
	 * hex_dump - Dumps a string into a traditional hex dump for programmers, in a format similar to the output of the BSD command hexdump -C file.
	 *			The default result is a string.
	 *			Based from  https://stackoverflow.com/questions/1057572/how-can-i-get-a-hex-dump-of-a-string-in-php
	 *
	 * @param string $string The string to dump.
	 *
	 * @param array $options Optional.  Hash that may contain one or more of the following to override (default values shown):
	 *                       [
	 *					'line_sep'       => "\n"	line separator character
	 *					'bytes_per_line' => 16
	 *					'pad_char'       => '.'		character to use for non-readable characters
	 *					'return'         => 'string' or 'array' defaults to string
	 *				]
	 * @return mixed String or array depending on 'return' config value.
	 */
	static function hex_dump( string $string, array $options = [] ) {
		if( ! is_scalar( $string ) ) {
			throw new InvalidArgumentException( 'string argument expected' );
		}
		if( ! is_array( $options ) ) {
			$options = [];
		}
		$line_sep       = isset( $options['line_sep'] )       ? $options['line_sep']          : "\n";
		$bytes_per_line = isset( $options['bytes_per_line'] ) ? $options['bytes_per_line']    : 16;
		$pad_char       = isset( $options['pad_char'] )       ? $options['pad_char']          : '.'; # padding for non-readable characters
		$return         = isset( $options['return'] )         ? $options['return']            : 'string';

		$text_lines     = str_split( $string, $bytes_per_line );
		$hex_lines      = str_split( bin2hex( $string ), $bytes_per_line * 2 );

		$offset = 0;
		$output = [];
		$bytes_per_line_div_2 = (int)( $bytes_per_line / 2 );
		foreach( $hex_lines as $i => $hex_line ) {
			$text_line = $text_lines[$i];
			$output[]  =
				sprintf( '%08X', $offset ).'  '.
				str_pad(
					strlen( $text_line ) > $bytes_per_line_div_2
					? implode( ' ', str_split( substr( $hex_line, 0, $bytes_per_line ), 2 ) ).'  '.implode( ' ', str_split( substr( $hex_line, $bytes_per_line ), 2 ) )
					: implode( ' ', str_split( $hex_line, 2 ) ), $bytes_per_line * 3 ).
				'  |'.preg_replace( '/[^\x20-\x7E]/', $pad_char, $text_line ).'|';
			$offset += $bytes_per_line;
		}
		$output[] = sprintf( '%08X', strlen( $string ) ).'  ('.strlen( $string ).' bytes)';
		return ( $return == 'string' ) ? join( $line_sep, $output ).$line_sep : $output;
	}

	/**
	 * getVariableName - returns the name of given variable as a string
	 *
	 * @param mixed $var The variable you want to name of.
	 *
	 * @return string The name of the given variable.
	 */
	static function getVariableName( $var ) {
		$varName = '';
		foreach( $GLOBALS as $key => $val ) {
			if( json_encode( $val ) == json_encode( $var ) ) {
				$varName = $key;
				break;
			}
		}
		return $varName;
	}

	/**
	 * getTypeAbbr - Convert the type string returned from gettype() to an abbreviated version
	 *
	 * @param string $type The type to convert.
	 *
	 * @return string The abbreviated type.
	 */
	private static function getTypeAbbr( $var ) : string {
		$abbreviation = '';
		switch( gettype( $var ) ) {
			# ordered by most likely used
			case 'integer':
				$abbreviation = self::ABBR_INTEGER;
				break;
			case 'string':
				$abbreviation = self::ABBR_STRING;
				break;
			case 'array':
				$abbreviation = self::ABBR_ARRAY;
				break;
			case 'NULL':
				$abbreviation = self::ABBR_NULL;
				break;
			case 'object':
				$abbreviation = self::ABBR_OBJECT;
				break;
			case 'boolean':
				$abbreviation = self::ABBR_BOOLEAN;
				break;
			case 'double':
				$abbreviation = self::ABBR_DOUBLE;
				break;
			case 'resource':
				$abbreviation = self::ABBR_RESOURCE;
				break;
			case 'unknown type':
				$abbreviation = self::ABBR_UNKNOWN;
				break;
			default:
				$abbreviation = "($var)";
				break;
		}
		return $abbreviation;
	}

	/**
	 * printR - like print_r but the name of the variable being dumped is shown (when possible), and types are shown like in var_dump()
	 *
	 * @param mixed   $var         The variable being dumped.
	 * @param boolean $showType    Show the type in the output. Defaults to true
	 * $param int     $indentLevel Do not pass in a value for this.  This method is recursive and passes this value to itself.
	 *
	 * @return void
	 */
	static function printR( $var, bool $showType = true, int &$indentLevel = 1 ) {
		$varType = self::getTypeAbbr( $var );
		$indent  = '    ';
		switch( $varType ) {
			case self::ABBR_ARRAY:
			case self::ABBR_OBJECT:
				$referenceIndicator = ( $varType == self::ABBR_ARRAY ? ' => ' : ' -> ' );
				$varName            = self::getVariableName( $var );
				if( $varName ) {
					echo $varName.$referenceIndicator;
				}

				#------------
				# open block
				#------------
				echo ( $varType == self::ABBR_ARRAY ) ? "[\n" : "{\n";
				#---------------
				# block contents
				#---------------
				#$indentValue = str_pad( "\t", $indentLevel, "\t" );
				$indentValue = str_repeat( $indent, $indentLevel );

				foreach( $var as $k => $v ) {
					$keyType = self::getTypeAbbr( $k );
					$valType = self::getTypeAbbr( $v );
					echo $indentValue.( $showType ? $keyType : '' ).$k.$referenceIndicator;
					if( $valType != self::ABBR_OBJECT && $valType != self::ABBR_ARRAY ) {
						echo ( $showType ? $valType : '' ).$v."\n";
					} else {
						$indentLevel++;
						self::printR( $v, $showType, $indentLevel );
					}
				}
				#-------------
				# close block
				#-------------
				$indentLevel--;
				$indentValue = str_repeat( $indent, $indentLevel );
				echo $indentValue.( ( $varType == self::ABBR_ARRAY ) ? ']' : '}' );
				break;

			case self::ABBR_RESOURCE:
			case self::ABBR_UNKNOWN:
				echo ( $showType ? $varType : '' ).self::getVariableName( $var ).': ';
				print_r( $var );
				break;

			default:
				echo self::getVariableName( $var ).': ';
				$valType = self::getTypeAbbr( $var );
				echo $valType.$var;
				break;
		}
		echo "\n";
	}
}