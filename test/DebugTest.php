<?php
exit;

require_once $_SERVER['APP_CONFIG'];
header( 'Content-Type: text/plain' );

echo "trace:\n";
echo BonsaiCode\Debug::trace( [ 'string' ] );

$varArray  = [ 'a' => 2, 'b' => [ 1, 3, 'efg', 5 => [ 'aa', 6 ] ], 'c' => NULL ];
$varObject = new DateTime();

$a = 'asd';
echo 'var name = '.BonsaiCode\Debug::getVariableName( $a )."\n";

BonsaiCode\Debug::printR( $a );
BonsaiCode\Debug::printR( $varArray );
BonsaiCode\Debug::printR( $varArray, false );
BonsaiCode\Debug::printR( $varObject );
BonsaiCode\Debug::printR( $varObject, false );

echo BonsaiCode\Debug::hex_dump( 'This is a test for hex_dump() returning a string.' );
print_r( BonsaiCode\Debug::hex_dump( 'This is a test for hex_dump() returning an array.', [ 'return' => 'array' ] ) );