# bonsaicode/debug
Debug utility class providing an improved version of print_r that shows variable types, and trace and hex_dump methods.

# Requirements
* PHP 7.1 or higher

# Installation
To install the module, run the following command in the console:

`$ composer require "bonsaicode/debug"`

# Usage

	<?php

	BonsaiCode\Debug::printR( $mixed );

	echo BonsaiCode\Debug::hex_dump( 'This is a test for hex_dump() returning a string.' );

	echo BonsaiCode\Debug::trace( [ 'string' ] );

	?>
